package com.twuc.webApp.domain.mazeRender.areaRenders;

import com.twuc.webApp.domain.mazeRender.AreaRender;

import java.awt.*;

/**
 * 该 {@link AreaRender} 将使用纯颜色渲染整个区域。
 */
public class AreaColorRender implements AreaRender {
    private final Color color;

    /**
     * 新建一个 {@link AreaRender} 实例。
     *
     * @param color 背景颜色。
     */
    public AreaColorRender(Color color) {
        this.color = color;
    }

    @Override
    public void render(Graphics2D graphics, Rectangle fullArea) {
        graphics.setColor(color);
        graphics.fillRect(fullArea.x, fullArea.y, fullArea.width, fullArea.height);
    }
}
